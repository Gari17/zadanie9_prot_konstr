function Student(name, point){
  this.name = name;
  this.point = point;
};

Student.prototype.show = function(){
  return console.log("Студент %s набрал %d баллов", this.name, this.point);
};


function StudentList(group, array){
  if(array == undefined){
    array = [];
  };
  for(var i = 0; i < (array.length); i+=2){
    this.add(array[i],array[i+1]);
  };
  this.nameGroup = group;
};

StudentList.prototype = Object.create(Array.prototype);
StudentList.prototype.contsructor = StudentList;
StudentList.prototype.add = function(name, point){
  this.push(new Student(name, point))
};


var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];

var hj2 = new StudentList("HJ-2", studentsAndPoints);


hj2.add("Александр Кравченко", 50);

//
var html7 = new StudentList("HTML-7");

html7.add("Олег Тишкин", 60);
html7.add("Александр Попов", 30);
//

StudentList.prototype.show = function(){
  console.log("Группа %s %d студентов:", this.nameGroup, this.length);
  this.forEach(function(student, i){
    student.show();
  });
};

html7.show();
hj2.show();


StudentList.prototype.transfer = function(name, group){
  var index = this.findIndex(function(stud){
    return stud.name == name;
  });
  group.push.apply(group, this.splice(index, 1));
};

hj2.transfer('Алексей Петров', HTML-7);
